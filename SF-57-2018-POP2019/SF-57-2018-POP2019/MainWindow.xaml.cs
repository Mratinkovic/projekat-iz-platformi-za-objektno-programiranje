﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SF_57_2018_POP2019.Models;


namespace SF_57_2018_POP2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
   
    public partial class MainWindow : Window
    {
        List<Osoba> users = new List<Osoba>();
        public MainWindow()
        {
            InitializeComponent();
            
            Administrator admin = new Administrator("Ognjen", "Andric", "mail@system.admin", "BUDI", "BUDI1", TipAccounta.Administrator, true);
            Profesor profa = new Profesor("Petar", "Peric", "peki@gmail.com", "profaPera", "profica123", TipAccounta.Profesor, true, null);
            Asistent asis = new Asistent("Sima", "Simic", "simke12@hotmail.com", "Simke", "simke123", TipAccounta.Asistent, true, profa);
            List<Asistent> asistentiProfesora = new List<Asistent>();
            asistentiProfesora.Add(asis);
            profa.Asistenti = asistentiProfesora;
            
            users.Add(admin); users.Add(profa); users.Add(asis);

           

            
                if (File.Exists(@"F:\fax\SF-57-2018-POP2019\SF-57-2018-POP2019\Logs\Users.txt"))
                {
                var lines = File.ReadAllLines(@"F:\fax\SF-57-2018-POP2019\SF-57-2018-POP2019\Logs\Users.txt");
                foreach (var line in lines)
                {
                    char sep = '|';
                    string[] lista = line.Split(sep);
                    string ime = lista[0];
                    string prezime = lista[1];
                    string email = lista[2];
                    string username = lista[3];
                    string sifra = lista[4];
                    Enum.TryParse(lista[5], out TipAccounta tipacc);
                    bool active = Convert.ToBoolean(lista[6]);
                    string provera = lista[7];
                    if(provera=="1"){
                        if (tipacc == TipAccounta.Profesor)
                        {
                            Profesor profesor1 = new Profesor(ime, prezime, email, username, sifra, tipacc, active, null);
                            users.Add(profesor1);
                        }
                        if (tipacc == TipAccounta.Asistent)
                        {
                            Asistent asistent1 = new Asistent(ime, prezime, email, username, sifra, tipacc, active, profa);
                            users.Add(asistent1);
                        }
                        if (tipacc == TipAccounta.Administrator)
                        {
                            Administrator admin1 = new Administrator(ime, prezime, email, username, sifra, tipacc, active);
                            users.Add(admin1);
                        }
                    }
                }
                    
                   /* 
*/
                    
                }
            dg_users.ItemsSource = users;


        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {

            Osoba osoba = (Osoba)dg_users.SelectedItem;
            Window1 window1 = new Window1();
            window1.ShowDialog();
                    
        }
                    

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            UserWindow userWindow = new UserWindow();
            userWindow.ShowDialog();
           

        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            Osoba osoba1 = (Osoba)dg_users.SelectedItem;
            var lines = File.ReadAllLines(@"F:\fax\SF-57-2018-POP2019\SF-57-2018-POP2019\Logs\Users.txt");
            File.WriteAllText(@"F:\fax\SF-57-2018-POP2019\SF-57-2018-POP2019\Logs\Users.txt", String.Empty);
            foreach (var line in lines)
            {
                
                char sep = '|';
                string[] lista = line.Split(sep);
                string ime = lista[0];
                string prezime = lista[1];
                string email = lista[2];
                string username = lista[3];
                string sifra = lista[4];
                Enum.TryParse(lista[5], out TipAccounta tipacc);
                bool active = Convert.ToBoolean(lista[6]);
                string provera = lista[7];
                if (osoba1.Username == username)
                {
                    provera = "0";
                }
                string upis = (ime + "|" + prezime + "|" + email + "|" + username + "|" + sifra + "|" + lista[5] + "|" + "True"+"|"+provera);



                using (StreamWriter w = File.AppendText(@"F:\fax\SF-57-2018-POP2019\SF-57-2018-POP2019\Logs\Users.txt"))
                {
                    w.WriteLine(upis);
                }


            }
            }

        private void Dg_users_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void Btn_ustanove_Click(object sender, RoutedEventArgs e)
        {
            PregledUstanova pregledUstanova = new PregledUstanova();
            pregledUstanova.ShowDialog();

        }
    }
}
