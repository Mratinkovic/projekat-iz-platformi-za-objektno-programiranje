﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_57_2018_POP2019.Models
{
    class Profesor:Osoba
    {
        private List<Asistent> asistenti;

        public List<Asistent> Asistenti
        {
            get { return asistenti; }
            set { asistenti = value; }
        }

        public Profesor(String ime, String prezime, String email, String username, String password, TipAccounta tip, bool active, List<Asistent> asistenti) : base(ime, prezime, email, username, password, tip, active)
        {
            this.Asistenti = asistenti;
        }


    }
}
