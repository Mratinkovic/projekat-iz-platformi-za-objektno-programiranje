﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_57_2018_POP2019.Models
{
    class Asistent:Osoba
    {
        private Profesor profesor;

        public Profesor Profesor
        {
            get { return profesor; }
            set { profesor = value; }
        }

        public Asistent(String ime, String prezime, String email, String username, String password, TipAccounta tip, bool active,Profesor profesor) : base(ime, prezime, email, username, password, tip, active)
        {
            this.Profesor = profesor;
        }

       
    }
}
