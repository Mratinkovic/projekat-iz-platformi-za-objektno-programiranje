﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF_57_2018_POP2019.Models
{
    class Ustanova
    {
        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }

        private int sifra;

        public int Sifra
        {
            get { return sifra; }
            set { sifra = value; }
        }

        private String lokacija;

        public String Lokacija
        {
            get { return lokacija; }
            set { lokacija = value; }
        }

        public Ustanova(int sifra,string naziv,string lokacija)
        {
            this.Sifra = sifra;
            this.Naziv = naziv;
            this.Lokacija = lokacija;
        }
    }
}
