﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SF_57_2018_POP2019.Models
{
    public class Osoba
    {
        private String ime;
        private String prezime;
        private String email;
        private String username;
        private String password;
        private TipAccounta tip;
        private bool active;
        public String Ime
        {
            get { return ime; }
            set { ime = value; }
        }
        public String Prezime
        {
            get { return prezime; }
            set { prezime = value; }
        }
        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        public String Username
        {
            get { return username; }
            set { username = value; }
        }
        public String Password
        {
            get { return password; }
            set { password = value; }
        }
        public TipAccounta Tip
        {
            get { return tip; }
            set { tip = value; }   
        }
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public  Osoba(String ime, String prezime, String email, String username, String password, TipAccounta tip, bool active)
            {
                this.Ime = ime;
                this.Prezime = prezime;
                this.Email = email;
                this.Username = username;
                this.Password = password;
                this.Tip = tip;
                this.Active = active;
        }
        public String __str__()
        {
            return this.Ime + " "+this.Prezime+" "+this.Email+" "+this.Username+" "+this.Password+" "+this.Tip.ToString()+" "+this.Active.ToString();
        }

    }
}
