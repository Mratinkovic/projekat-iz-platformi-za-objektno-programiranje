﻿using SF_57_2018_POP2019.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF_57_2018_POP2019
{
    /// <summary>
    /// Interaction logic for PregledUstanova.xaml
    /// </summary>
    public partial class PregledUstanova : Window
    {
        List<Ustanova> ustanove = new List<Ustanova>();
        public PregledUstanova()
        {
            InitializeComponent();
            Ustanova ustanova1 = new Ustanova(1, "Ustanova1", "Jugodrvo");
            Ustanova ustanova2 = new Ustanova(1, "Ustanova2", "Jugodrvo1");
            Ustanova ustanova3 = new Ustanova(1, "Ustanova3", "Jugodrvo2");
            ustanove.Add(ustanova1);
            ustanove.Add(ustanova2);
            ustanove.Add(ustanova3);
            dg_ustanove.ItemsSource=ustanove;
            
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Ustanova editUstanova = (Ustanova)dg_ustanove.SelectedItem;
            if (txt_naziv.Text != " ") {
                editUstanova.Naziv = txt_naziv.Text;
                    }
            if(txt_sifra.Text != " ") {
                int a;
                Int32.TryParse(txt_sifra.Text, out a);
                editUstanova.Sifra = a;
            }
            if (txt_lokacija.Text != " ")
            {
                editUstanova.Lokacija = txt_lokacija.Text;
            }

        }

        private void Add_ustanove_Click(object sender, RoutedEventArgs e)
        {
            int sifra;
            string naziv = txt_naziv.Text;
            string lokacija = txt_lokacija.Text;
            Int32.TryParse( txt_sifra.Text,out sifra);
            Ustanova ustanovaAdd = new Ustanova(sifra, naziv, lokacija);
            ustanove.Add(ustanovaAdd);
            dg_ustanove.ItemsSource = ustanove;
            PregledUstanova pregledUstanova = new PregledUstanova();
            pregledUstanova.ShowDialog();
        }
    }
}
